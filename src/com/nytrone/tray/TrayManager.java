package com.nytrone.tray;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;

import javax.swing.JFrame;

import com.nytrone.frame.NytroneFrame;
import com.nytrone.utils.Utils;

public class TrayManager {

	public TrayManager(NytroneFrame frame) {
		if (!SystemTray.isSupported()) {
			System.out.println("SystemTray is not supported");
			return;
		}
		final PopupMenu popup = new PopupMenu();
		final SystemTray tray = SystemTray.getSystemTray();
		final TrayIcon trayIcon = new TrayIcon(Utils.getScaledImage(Utils.getFileAsBufferedImage("LogoIcon.png"),
				(int)tray.getTrayIconSize().getWidth(), (int)tray.getTrayIconSize().getHeight()));

		MenuItem aboutItem = new MenuItem("About");
		MenuItem settingsItem = new MenuItem("Settings");
		MenuItem exitItem = new MenuItem("Exit");

		popup.add(aboutItem);
		popup.addSeparator();
		popup.add(settingsItem);
		popup.addSeparator();
		popup.add(exitItem);

		trayIcon.setPopupMenu(popup);

		trayIcon.addMouseListener(new java.awt.event.MouseAdapter() {

		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        if (evt.getClickCount() == 2) {
		            frame.setVisible(true);
		            frame.setExtendedState(JFrame.NORMAL);
		        }
		    }

		});
		
		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			System.out.println("TrayIcon could not be added.");
			return;
		}
	}

}
