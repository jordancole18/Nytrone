package com.nytrone.utils;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

import com.nytrone.frame.NytroneFrame;

public class Animation {

	private static boolean transition;
	
	public static void transitionFrom(JPanel panel, JPanel ssp, NytroneFrame frame) {
		if(transition) return;
		transition = true;
		frame.add(ssp);
		ssp.setLocation(0, 0);
		Timer t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			int num = 1;
			@Override
			public void run() {
				
				if(num >= panel.getWidth()) {
					frame.setPage(ssp);
					transition = false;
					this.cancel();
				}
				
				panel.setLocation(-num, panel.getY());
				num++;
			}
			
			
		}, 1, 1);
	}
	
	public static void reverseTransitionFrom(JPanel panel, JPanel ssp, NytroneFrame frame) {
		if(transition) return;
		transition = true;
		frame.add(ssp);
		ssp.setLocation(0, 0);
		Timer t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			int num = 1;
			@Override
			public void run() {
				
				if(num >= panel.getWidth()) {
					frame.setPage(ssp);
					transition = false;
					this.cancel();
				}
				
				panel.setLocation(num, panel.getY());
				num++;
			}
			
			
		}, 1, 1);
	}
	
}
