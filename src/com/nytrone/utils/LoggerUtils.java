package com.nytrone.utils;

import java.util.logging.Logger;

import com.nytrone.Nytrone;

public class LoggerUtils {

	public static void log(String message) {
		Nytrone.logger.info(message);
	}
	
	public static void logError(String message) {
		Logger.getGlobal().severe(message);
	}
	
}
