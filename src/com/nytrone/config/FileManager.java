package com.nytrone.config;

import java.io.File;
import java.io.IOException;

import org.simpleyaml.configuration.file.FileConfiguration;
import org.simpleyaml.configuration.file.YamlConfiguration;

public class FileManager {

	static FileManager instance = new FileManager();

	File dataFolder = new File(System.getProperty("user.home") + "/.Nytrone");

	FileConfiguration config;
	File cfile;
	FileConfiguration tempData;
	File tfile;

	public static FileManager getInstance() {
		return instance;
	}

	public void setup() {
		cfile = new File(dataFolder, "config.yml");
		config = YamlConfiguration.loadConfiguration(cfile);

		tfile = new File(dataFolder, "tempData.yml");
		tempData = YamlConfiguration.loadConfiguration(tfile);

		if (!(tfile.exists())) {
			try {
				tempData.save(tfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (!(cfile.exists())) {
			try {
				config.save(cfile);
			} catch (IOException e) {

			}
		}

	}

	public FileConfiguration getConfig() {
		return config;
	}

	public FileConfiguration getTempData() {
		return tempData;
	}

	// Save the file
	public void saveConfig() throws IOException {
		config.save(cfile);
	}

	public void saveTempData() throws IOException {
		tempData.save(tfile);
	}

	// Reload the file
	public void reloadConfig() {
		config = YamlConfiguration.loadConfiguration(cfile);
	}

	public void reloadTempData() {
		tempData = YamlConfiguration.loadConfiguration(tfile);
	}

}