package com.nytrone.net.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	private Socket connection;
	private String message;
	private ObjectOutputStream output;
	private ObjectInputStream input;

	public Client() {
	}

	public void start() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				connectToServer();
			}
		});
		t.start();
	}

	public void connectToServer() {
		try {
			this.connection = new Socket("127.0.0.1", 8222);
			this.setupStreams();
			this.whileChatting();
		} catch (UnknownHostException var8) {
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException var7) {
				var7.printStackTrace();
			}

			this.connectToServer();
		} catch (IOException var9) {
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException var6) {
				var6.printStackTrace();
			}

			this.connectToServer();
		}

	}

	public void setupStreams() throws IOException {
		this.output = new ObjectOutputStream(this.connection.getOutputStream());
		this.output.flush();
		this.input = new ObjectInputStream(this.connection.getInputStream());
	}

	public void whileChatting() throws IOException {
		do {
			try {
				this.message = (String) this.input.readObject();

				if (message.startsWith("chat:")) {
					
				}

			} catch (ClassNotFoundException var9) {

			}
		} while (!this.message.equals("END"));

	}

	public void sendHostMessage(String message) {
		try {
			this.output.writeObject(message);
			this.output.flush();
		} catch (IOException var3) {
			
		}

	}
}
