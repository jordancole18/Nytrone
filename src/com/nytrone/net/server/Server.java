package com.nytrone.net.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;

import com.nytrone.net.NetworkManager;

public class Server {

	private NetworkManager networkManager;
	private ServerSocket serverSocket;
	private Socket acceptingSocket;
	private List<ServerConnection> serverConnections;
	
	public Server(NetworkManager networkManager) {
		this.networkManager = networkManager;
		this.serverConnections = new ArrayList<ServerConnection>();
		ServerSocketFactory ssocketFactory = SSLServerSocketFactory.getDefault();
		try {
			serverSocket = ssocketFactory.createServerSocket(networkManager.getPort());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void acceptConnections() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					serverSocket = new ServerSocket(8222, 10, InetAddress.getByName("localhost"));
					while(true){
						try {
							acceptingSocket = serverSocket.accept();
						} catch (IOException e) {
							System.out.println("I/O error: " + e);
						}
						ServerConnection thread = new ServerConnection(networkManager, acceptingSocket);
						serverConnections.add(thread);
						thread.start();
						System.out.println("Server Added!");
					}
				} catch (IOException localIOException1) {
				}
			}
		});
		t.start();
	}
	
	public NetworkManager getNetworkManager() {
		return networkManager;
	}
	
	public ServerSocket getServerSocket() {
		return serverSocket;
	}
	
	public synchronized List<ServerConnection> getServerConnections(){
		return serverConnections;
	}
	
}
