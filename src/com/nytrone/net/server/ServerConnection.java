package com.nytrone.net.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.nytrone.net.NetworkManager;

public class ServerConnection extends Thread {

	private NetworkManager networkManager;
	public Socket socket;
	public ObjectOutputStream output;
	public ObjectInputStream input;

	public ServerConnection(NetworkManager networkManager, Socket connected) {
		this.socket = connected;
	}

	public NetworkManager getNetworkManage() {
		return networkManager;
	}
	
	public void run() {
		String message = "";
		if (this.output == null || this.input == null) {
			try {
				this.output = new ObjectOutputStream(this.socket.getOutputStream());
				this.output.flush();
				this.input = new ObjectInputStream(this.socket.getInputStream());
			} catch (IOException var10) {
				;
			}
		}

		while (true) {

			do {
				try {
					if (this.input != null) {
						message = (String) this.input.readObject();
						System.out.println("From Server: " + message);
						//FileManager fm = FileManager.getInstance();

						if (message.startsWith("channel:")) {

						}

					}
				} catch (ClassNotFoundException var11) {

				} catch (IOException var12) {

				}
			} while (!message.equalsIgnoreCase("stopserverend"));

		}
	}

	public void sendMessage(String message) {
		if (socket.isConnected() == false) {
			try {
				socket.close();
			} catch (IOException e) {
			}
			return;
		}
		try {
			this.output.writeObject(message);
			this.output.flush();
		} catch (IOException var3) {
		}

	}

}
