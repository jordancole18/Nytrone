package com.nytrone.net;

import com.nytrone.config.FileManager;
import com.nytrone.frame.NytroneFrame;
import com.nytrone.net.server.Server;
import com.nytrone.utils.LoggerUtils;

public class NetworkManager {

	private NytroneFrame frame;
	private NetworkType networkType;
	private Server server;
	//private Client client;	
	
	public NetworkManager(NytroneFrame frame) {
		this.frame = frame;
	}
	
	public NytroneFrame getFrame() {
		return frame;
	}
	
	public NetworkType getType() {
		return networkType;
	}
	
	public boolean init() {
		if(FileManager.getInstance().getConfig().contains("settings.type")) {
			String type = FileManager.getInstance().getConfig().getString("settings.type");
			boolean val = true;
			
			if(type.equalsIgnoreCase("server")) {
				networkType = NetworkType.SERVER;
				setupServer();
			}else if(type.equalsIgnoreCase("client")) {
				networkType = NetworkType.SERVER;
				setupClient();
			}else {
				val = false;
			}
			
			return val;
		}else {
			return false;
		}
	}
	
	public void setupServer() {
		LoggerUtils.log("Starting Server...");
		server = new Server(this);
	}
	
	public void setupClient() {
		LoggerUtils.log("Starting Client...");
	}

	public Server getServer() {
		return server;
	}
	
	public int getPort() {
		if(FileManager.getInstance().getConfig().contains("settings.client.port")) {
			return FileManager.getInstance().getConfig().getInt("settings.client.port");
		}
		return FileManager.getInstance().getConfig().getInt("settings.server.port");
	}
	
}
