package com.nytrone.frame.pages.server;

import java.awt.Color;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.nytrone.Nytrone;
import com.nytrone.config.FileManager;
import com.nytrone.frame.NytroneFrame;
import com.nytrone.frame.pages.PCNamePage;
import com.nytrone.frame.pages.SetupPage;
import com.nytrone.utils.Animation;
import com.nytrone.utils.RandomString;
import com.nytrone.utils.Utils;

public class ServerSetupPage extends JPanel {

	private static final long serialVersionUID = 7786304989813542844L;

	private NytroneFrame frame;

	private JButton backBtn;
	private JLabel logoImg;

	private JLabel serverIPWarning;
	private JLabel serverIPLabel;
	private JFormattedTextField serverIP;

	private JLabel serverPortWarning;
	private JLabel serverPortLabel;
	private JTextField serverPort;

	private JLabel serverCodeWarning;
	private JLabel serverCodeLabel;
	private JTextField serverCode;
	private JButton serverCodeReset;
	
	private JButton continueBtn;
	
	public ServerSetupPage(NytroneFrame frame) {
		this.frame = frame;
		setSize(frame.getSize());
		setLayout(null);

		logoImg = new JLabel();
		logoImg.setIcon(new ImageIcon(Utils.getScaledImage(Utils.getFileAsBufferedImage("Logo.png"), 300, 113)));
		logoImg.setSize(300, 113);
		logoImg.setLocation(((this.getWidth() / 2) - (logoImg.getWidth() / 2)), 30);

		serverIPLabel = new JLabel("IP", SwingConstants.CENTER);
		serverIPLabel.setSize(200, 30);
		serverIPLabel.setLocation((this.getWidth() / 2) - (serverIPLabel.getWidth() / 2),
				((this.getHeight() / 2) - (serverIPLabel.getHeight() / 2)) - 130);

		serverIPWarning = new JLabel(new ImageIcon(Utils.getFileAsBufferedImage("warningIcon.png")));
		serverIPWarning.setSize(24, 24);
		serverIPWarning.setLocation(((this.getWidth() / 2) - (serverIPWarning.getWidth() / 2)) + 115,
				((this.getHeight() / 2) - (serverIPWarning.getHeight() / 2)) - 100);
		serverIPWarning.setVisible(false);

		serverIP = new JFormattedTextField(Utils.getIP());
		serverIP.setHorizontalAlignment(JTextField.CENTER);
		serverIP.setSize(200, 25);
		serverIP.setLocation((this.getWidth() / 2) - (serverIP.getWidth() / 2),
				((this.getHeight() / 2) - (serverIP.getHeight() / 2)) - 100);
		serverIP.setCaretPosition(serverIP.getDocument().getLength());
		serverIP.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		serverIP.setFont(Nytrone.TAHOMA.deriveFont(11.0f));

		serverPortLabel = new JLabel("Port", SwingConstants.CENTER);
		serverPortLabel.setSize(200, 30);
		serverPortLabel.setLocation((this.getWidth() / 2) - (serverPortLabel.getWidth() / 2),
				((this.getHeight() / 2) - (serverPortLabel.getHeight() / 2)) - 50);

		serverPortWarning = new JLabel(new ImageIcon(Utils.getFileAsBufferedImage("warningIcon.png")));
		serverPortWarning.setSize(24, 24);
		serverPortWarning.setLocation(((this.getWidth() / 2) - (serverPortWarning.getWidth() / 2)) + 115,
				((this.getHeight() / 2) - (serverPortWarning.getHeight() / 2)) - 20);
		serverPortWarning.setVisible(false);
		
		serverPort = new JTextField("6782");
		serverPort.setHorizontalAlignment(JTextField.CENTER);
		serverPort.setSize(200, 25);
		serverPort.setLocation((this.getWidth() / 2) - (serverPort.getWidth() / 2),
				((this.getHeight() / 2) - (serverPort.getHeight() / 2)) - 20);
		serverPort.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		serverPort.setFont(Nytrone.TAHOMA.deriveFont(11.0f));
		
		serverCodeLabel = new JLabel("Code", SwingConstants.CENTER);
		serverCodeLabel.setSize(200, 30);
		serverCodeLabel.setLocation((this.getWidth() / 2) - (serverCodeLabel.getWidth() / 2),
				((this.getHeight() / 2) - (serverCodeLabel.getHeight() / 2)) + 30);

		serverCodeWarning = new JLabel(new ImageIcon(Utils.getFileAsBufferedImage("warningIcon.png")));
		serverCodeWarning.setSize(24, 24);
		serverCodeWarning.setLocation(((this.getWidth() / 2) - (serverCodeWarning.getWidth() / 2)) + 115,
				((this.getHeight() / 2) - (serverCodeWarning.getHeight() / 2)) + 60);
		serverCodeWarning.setVisible(false);

		serverCode = new JTextField(new RandomString(6).nextString());
		serverCode.setHorizontalAlignment(JTextField.CENTER);
		serverCode.setSize(200, 25);
		serverCode.setLocation((this.getWidth() / 2) - (serverPort.getWidth() / 2),
				((this.getHeight() / 2) - (serverPort.getHeight() / 2)) + 60);
		serverCode.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		serverCode.setFont(Nytrone.TAHOMA.deriveFont(11.0f));

		serverCodeReset = new JButton();
		serverCodeReset.setSize(24, 24);
		serverCodeReset.setRolloverEnabled(true);
		serverCodeReset.setRolloverIcon(new ImageIcon(Utils.getFileAsBufferedImage("resetBtnHover.png")));
		serverCodeReset.setIcon(new ImageIcon(Utils.getFileAsBufferedImage("resetBtn.png")));
		serverCodeReset.setContentAreaFilled(false);
		serverCodeReset.setFocusPainted(false);
		serverCodeReset.setBorderPainted(false);
		serverCodeReset.setLocation(((this.getWidth() / 2) - (serverCodeReset.getWidth() / 2)) + 115,
				((this.getHeight() / 2) - (serverCodeReset.getHeight() / 2)) + 60);
		serverCodeReset.addActionListener((e)->{
			serverCode.setText(new RandomString(6).nextString());
		});
		
		backBtn = new JButton();
		backBtn.setSize(32, 32);
		backBtn.setLocation(16, 16);
		backBtn.setRolloverEnabled(true);
		backBtn.setRolloverIcon(
				new ImageIcon(Utils.getScaledImage(Utils.getFileAsBufferedImage("backBtnHover.png"), 32, 32)));
		backBtn.setIcon(new ImageIcon(Utils.getScaledImage(Utils.getFileAsBufferedImage("backBtn.png"), 32, 32)));
		backBtn.setContentAreaFilled(false);
		backBtn.setFocusable(false);
		backBtn.setBorderPainted(false);
		backBtn.addActionListener((e) -> {
			Animation.reverseTransitionFrom(this, new SetupPage(frame), frame);
		});

		continueBtn = new JButton("CONTINUE");
		continueBtn.setFocusPainted(false);
		continueBtn.setBackground(Nytrone.DEFAULT_COLOR);
		continueBtn.setForeground(Color.WHITE);
		continueBtn.setBorder(BorderFactory.createLineBorder(new Color(78, 83, 84), 1, false));
		continueBtn.setSize(300, 40);
		continueBtn.setFont(Nytrone.DEFAULT_FONT);
		continueBtn.setLocation(((this.getWidth() / 2) - (continueBtn.getWidth() / 2)),
				((this.getHeight() / 2) - (continueBtn.getHeight() / 2)) + 150);
		continueBtn.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				if (continueBtn.getModel().isPressed()) {
					continueBtn.setBackground(Nytrone.DEFAULT_COLOR);
				} else if (continueBtn.getModel().isRollover()) {
					continueBtn.setBackground(Nytrone.DEFAULT_COLOR.darker());
				} else {
					continueBtn.setBackground(Nytrone.DEFAULT_COLOR);
				}
			}
		});

		continueBtn.addActionListener((e)->{
			if(validateField()) {
				
				FileManager fm = FileManager.getInstance();
				
				fm.getConfig().set("settings.type", "server");
				fm.getConfig().set("settings.server.ip", serverIP.getText());
				fm.getConfig().set("settings.server.port", Integer.parseInt(serverPort.getText()));
				fm.getConfig().set("settings.server.code", serverCode.getText());
				try {
					fm.saveConfig();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Animation.transitionFrom(this, new PCNamePage(frame, this), frame);
			}
		});
		
		add(backBtn);
		add(logoImg);
		add(serverIPLabel);
		add(serverIPWarning);
		add(serverIP);
		add(serverPortLabel);
		add(serverPortWarning);
		add(serverPort);
		add(serverCodeLabel);
		add(serverCodeWarning);
		add(serverCode);
		add(serverCodeReset);
		add(continueBtn);
	}

	public ServerSetupPage(NytroneFrame frame, boolean getFromCfg) {
		this(frame);
		if(getFromCfg) {
			serverIP.setText(FileManager.getInstance().getConfig().getString("settings.server.ip"));
			serverPort.setText(FileManager.getInstance().getConfig().getString("settings.server.port"));
			serverCode.setText(FileManager.getInstance().getConfig().getString("settings.server.code"));
		}
	}
	
	public boolean validateField() {
		boolean ip = false;
		boolean port = false;
		
		if(!Utils.isIP(serverIP.getText())) {
			serverIPWarning.setVisible(true);
			ip = false;
		}else {
			serverIPWarning.setVisible(false);
			ip = true;
		}
		
		if(!Utils.isInt(serverPort.getText())) {
			serverPortWarning.setVisible(true);
			port = false;
		}else {
			serverPortWarning.setVisible(false);
			port = true;
		}
		
		if(ip && port) {
			return true;
		}else {
			return false;
		}
	}

	public NytroneFrame getFrame() {
		return frame;
	}

}
