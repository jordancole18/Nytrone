package com.nytrone.frame.pages;

import java.awt.Color;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.nytrone.Nytrone;
import com.nytrone.config.FileManager;
import com.nytrone.frame.NytroneFrame;
import com.nytrone.frame.pages.server.ClientSetupPage;
import com.nytrone.frame.pages.server.ServerSetupPage;
import com.nytrone.utils.Animation;
import com.nytrone.utils.Utils;

public class PCNamePage extends JPanel{

	private static final long serialVersionUID = 7786304989813542844L;

	private NytroneFrame frame;

	private JButton backBtn;
	private JLabel logoImg;

	private JLabel pcNameLabel;
	private JFormattedTextField pcName;

	private JButton continueBtn;
	
	public PCNamePage(NytroneFrame frame, JPanel from) {
		this.frame = frame;
		setSize(frame.getSize());
		setLayout(null);

		logoImg = new JLabel();
		logoImg.setIcon(new ImageIcon(Utils.getScaledImage(Utils.getFileAsBufferedImage("Logo.png"), 300, 113)));
		logoImg.setSize(300, 113);
		logoImg.setLocation(((this.getWidth() / 2) - (logoImg.getWidth() / 2)), 30);

		pcNameLabel = new JLabel("What is the name of your PC?", SwingConstants.CENTER);
		pcNameLabel.setSize(200, 30);
		pcNameLabel.setLocation((this.getWidth() / 2) - (pcNameLabel.getWidth() / 2),
				((this.getHeight() / 2) - (pcNameLabel.getHeight() / 2)) - 130);

		pcName = new JFormattedTextField(Utils.getComputerName());
		pcName.setHorizontalAlignment(JTextField.CENTER);
		pcName.setSize(200, 25);
		pcName.setLocation((this.getWidth() / 2) - (pcName.getWidth() / 2),
				((this.getHeight() / 2) - (pcName.getHeight() / 2)) - 100);
		pcName.setCaretPosition(pcName.getDocument().getLength());
		pcName.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		pcName.setFont(Nytrone.TAHOMA.deriveFont(11.0f));
		
		backBtn = new JButton();
		backBtn.setSize(32, 32);
		backBtn.setLocation(16, 16);
		backBtn.setRolloverEnabled(true);
		backBtn.setRolloverIcon(
				new ImageIcon(Utils.getScaledImage(Utils.getFileAsBufferedImage("backBtnHover.png"), 32, 32)));
		backBtn.setIcon(new ImageIcon(Utils.getScaledImage(Utils.getFileAsBufferedImage("backBtn.png"), 32, 32)));
		backBtn.setContentAreaFilled(false);
		backBtn.setFocusable(false);
		backBtn.setBorderPainted(false);
		backBtn.addActionListener((e) -> {
			if(from.getClass().getName().contains("Server")) {
				Animation.reverseTransitionFrom(this, new ServerSetupPage(frame, true), frame);
			}else {
				Animation.reverseTransitionFrom(this, new ClientSetupPage(frame, true), frame);
			}
		});

		continueBtn = new JButton("CONTINUE");
		continueBtn.setFocusPainted(false);
		continueBtn.setBackground(Nytrone.DEFAULT_COLOR);
		continueBtn.setForeground(Color.WHITE);
		continueBtn.setBorder(BorderFactory.createLineBorder(new Color(78, 83, 84), 1, false));
		continueBtn.setSize(300, 40);
		continueBtn.setFont(Nytrone.DEFAULT_FONT);
		continueBtn.setLocation(((this.getWidth() / 2) - (continueBtn.getWidth() / 2)),
				((this.getHeight() / 2) - (continueBtn.getHeight() / 2)) + 75);
		continueBtn.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				if (continueBtn.getModel().isPressed()) {
					continueBtn.setBackground(Nytrone.DEFAULT_COLOR);
				} else if (continueBtn.getModel().isRollover()) {
					continueBtn.setBackground(Nytrone.DEFAULT_COLOR.darker());
				} else {
					continueBtn.setBackground(Nytrone.DEFAULT_COLOR);
				}
			}
		});

		continueBtn.addActionListener((e)->{
			if(pcName.getText() != null) {
				if(!pcName.getText().equalsIgnoreCase("")) {
					FileManager.getInstance().getConfig().set("settings.pcName", pcName.getText());
					try {
						FileManager.getInstance().saveConfig();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		
		add(backBtn);
		add(logoImg);
		add(pcNameLabel);
		add(pcName);
		add(continueBtn);
	}

	public NytroneFrame getFrame() {
		return frame;
	}

}
