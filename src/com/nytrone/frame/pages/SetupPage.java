package com.nytrone.frame.pages;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.nytrone.Nytrone;
import com.nytrone.frame.NytroneFrame;
import com.nytrone.frame.pages.server.ClientSetupPage;
import com.nytrone.frame.pages.server.ServerSetupPage;
import com.nytrone.utils.Animation;
import com.nytrone.utils.Utils;

public class SetupPage extends JPanel {

	private static final long serialVersionUID = 1L;

	private NytroneFrame frame;
	private JLabel logoImg;
	private JButton serverBtn, clientBtn;

	public SetupPage(NytroneFrame frame) {
		this.frame = frame;
		setSize(frame.getSize());
		setLayout(null);

		logoImg = new JLabel();
		logoImg.setIcon(new ImageIcon(Utils.getScaledImage(Utils.getFileAsBufferedImage("Logo.png"), 300, 113)));
		logoImg.setSize(300, 113);
		logoImg.setLocation(((this.getWidth() / 2) - (logoImg.getWidth() / 2)), 30);

		serverBtn = new JButton("I AM THE SERVER.");
		serverBtn.setFocusPainted(false);
		serverBtn.setBackground(Nytrone.DEFAULT_COLOR);
		serverBtn.setForeground(Color.WHITE);
		serverBtn.setBorder(BorderFactory.createLineBorder(new Color(78, 83, 84), 1, false));
		serverBtn.setSize(400, 50);
		serverBtn.setFont(Nytrone.DEFAULT_FONT);
		serverBtn.setLocation(((this.getWidth() / 2) - (serverBtn.getWidth() / 2)),
				((this.getHeight() / 2) - (serverBtn.getHeight() / 2)) - 100);
		serverBtn.addActionListener((e)->{
			Animation.transitionFrom(this, new ServerSetupPage(frame), frame);
		});
		serverBtn.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				if (serverBtn.getModel().isPressed()) {
					serverBtn.setBackground(Nytrone.DEFAULT_COLOR);
				} else if (serverBtn.getModel().isRollover()) {
					serverBtn.setBackground(Nytrone.DEFAULT_COLOR.darker());
				} else {
					serverBtn.setBackground(Nytrone.DEFAULT_COLOR);
				}
			}
		});

		clientBtn = new JButton("I AM THE CLIENT.");
		clientBtn.setFocusPainted(false);
		clientBtn.setBackground(Nytrone.DEFAULT_COLOR);
		clientBtn.setForeground(Color.WHITE);
		clientBtn.setBorder(BorderFactory.createLineBorder(new Color(78, 83, 84), 1, false));
		clientBtn.setSize(400, 50);
		clientBtn.setFont(Nytrone.DEFAULT_FONT);
		clientBtn.setLocation(((this.getWidth() / 2) - (clientBtn.getWidth() / 2)),
				((this.getHeight() / 2) - (clientBtn.getHeight() / 2)));
		clientBtn.addActionListener((e)->{
			Animation.transitionFrom(this, new ClientSetupPage(frame), frame);
		});
		clientBtn.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				if (clientBtn.getModel().isPressed()) {
					clientBtn.setBackground(Nytrone.DEFAULT_COLOR);
				} else if (clientBtn.getModel().isRollover()) {
					clientBtn.setBackground(Nytrone.DEFAULT_COLOR.darker());
				} else {
					clientBtn.setBackground(Nytrone.DEFAULT_COLOR);
				}
			}
		});

		
		add(logoImg);
		add(serverBtn);
		add(clientBtn);
	}

	public NytroneFrame getFrame() {
		return frame;
	}
	
}
