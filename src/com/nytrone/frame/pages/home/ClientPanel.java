package com.nytrone.frame.pages.home;

import javax.swing.JPanel;

import com.nytrone.frame.NytroneFrame;

public class ClientPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private NytroneFrame frame;

	public ClientPanel(NytroneFrame frame) {
		this.frame = frame;
	}

	public NytroneFrame getFrame() {
		return frame;
	}
	
}
