package com.nytrone.frame.pages.home;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.MenuSelectionManager;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingConstants;

import com.nytrone.Nytrone;
import com.nytrone.config.FileManager;
import com.nytrone.frame.NytroneFrame;
import com.nytrone.frame.components.ComputerScrollBar;
import com.nytrone.frame.components.NetworkComputerComponent;
import com.nytrone.utils.RandomString;
import com.nytrone.utils.Utils;

public class ServerPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private NytroneFrame frame;
	private JMenuBar menuBar;
	private JLabel serverCodeLabel, connectedComputersLabel;
	private JTextField serverCode;
	private JButton serverCodeReset;

	public ServerPanel(NytroneFrame frame) {
		this.frame = frame;
		setSize(frame.getSize());
		setLayout(null);

		JPanel computerPanel = new JPanel();
		computerPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY, 2));
		computerPanel.setSize(300, 2000);
		computerPanel.setLayout(new BoxLayout(computerPanel, BoxLayout.Y_AXIS));

		for (int i = 0; i < 20; i++) {
			NetworkComputerComponent comp = new NetworkComputerComponent("TEST COMPUTER #" + i);
			// comp.setLocation(0, i * 75);
			computerPanel.add(comp);
			if (i != 9) {
				computerPanel.add(Box.createRigidArea(new Dimension(0, 17)));
			}
		}

		Utils.toggleSystemLook();
		menuBar = new JMenuBar();
		menuBar.setSize(500, 50);
		menuBar.setBorderPainted(false);
		menuBar.setBackground(Color.WHITE);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setSize(frame.getSize());
		add(panel);

		JMenu file = new JMenu("File");
		JMenuItem exit = new JMenuItem("Exit");
		exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, 2, false));
		exit.setPreferredSize(new Dimension(200, 20));
		exit.setHorizontalAlignment(SwingConstants.LEFT);
		exit.setHorizontalTextPosition(JMenuItem.LEFT);
		exit.addActionListener((e) -> System.exit(0));
		// file.addSeparator();
		file.add(exit);
		menuBar.add(file);

		JMenu settings = new JMenu("Settings");

		JMenuItem checkUpdate = new JMenuItem("Check for Updates");
		checkUpdate.setPreferredSize(new Dimension(200, 20));
		checkUpdate.setHorizontalAlignment(SwingConstants.LEFT);
		checkUpdate.setHorizontalTextPosition(JMenuItem.LEFT);
		checkUpdate.addActionListener((e) -> {

		});
		settings.add(checkUpdate);
		settings.addSeparator();

		JMenuItem serverSettings = new JMenuItem("Server Settings");
		serverSettings.setPreferredSize(new Dimension(200, 20));
		serverSettings.setHorizontalAlignment(SwingConstants.LEFT);
		serverSettings.setHorizontalTextPosition(JMenuItem.LEFT);
		serverSettings.addActionListener((e) -> {

		});
		settings.add(serverSettings);

		JMenuItem clientSettings = new JMenuItem("Switch to Client");
		clientSettings.setPreferredSize(new Dimension(200, 20));
		clientSettings.setHorizontalAlignment(SwingConstants.LEFT);
		clientSettings.setHorizontalTextPosition(JMenuItem.LEFT);
		clientSettings.addActionListener((e) -> {

		});
		
		JMenuItem monitor = new JMenuItem("Monitor Arrangement");
		monitor.setPreferredSize(new Dimension(200, 20));
		monitor.setHorizontalAlignment(SwingConstants.LEFT);
		monitor.setHorizontalTextPosition(JMenuItem.LEFT);
		monitor.addActionListener((e) -> {

		});
		
		settings.add(serverSettings);
		settings.add(clientSettings);
		settings.add(monitor);
		settings.addSeparator();

		JMenuItem registerLicense = new JMenuItem("Register License");
		registerLicense.setPreferredSize(new Dimension(200, 20));
		registerLicense.setHorizontalAlignment(SwingConstants.LEFT);
		registerLicense.setHorizontalTextPosition(JMenuItem.LEFT);
		registerLicense.addActionListener((e) -> {

		});
		settings.add(registerLicense);
		menuBar.add(settings);

		frame.setJMenuBar(menuBar);

		ComputerScrollBar scrollPane = new ComputerScrollBar(computerPanel);
		scrollPane.setLocation(450, 100);
		scrollPane.setSize(321, 400);
		ScrollPaneLayout layout = new ScrollPaneLayout();
		scrollPane.setLayout(layout);
		scrollPane.setBorder(null);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(22);
		// scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		// scrollPane.setBorder(BorderFactory.createLineBorder(Nytrone.DEFAULT_COLOR.darker(),
		// 2));
		panel.add(scrollPane);

		Utils.toggleSystemLook();

		serverCodeLabel = new JLabel("Code", SwingConstants.CENTER);
		serverCodeLabel.setSize(100, 30);
		serverCodeLabel.setLocation((400 / 2) - (serverCodeLabel.getWidth() / 2), 10);
		serverCodeLabel.setFont(Nytrone.INSTALL_FONT.deriveFont(20.0f));

		connectedComputersLabel = new JLabel("Connected Computers", SwingConstants.CENTER);
		connectedComputersLabel.setSize(200, 30);
		connectedComputersLabel.setLocation(500, 10);
		connectedComputersLabel.setFont(Nytrone.INSTALL_FONT.deriveFont(20.0f));

		serverCode = new JTextField(FileManager.getInstance().getConfig().getString("settings.server.code"));
		serverCode.setHorizontalAlignment(JTextField.CENTER);
		serverCode.setEditable(false);
		serverCode.setSelectionColor(Nytrone.DEFAULT_COLOR);
		serverCode.setSelectedTextColor(Color.white);
		serverCode.setSize(100, 50);
		serverCode.setLocation((400 / 2) - (serverCode.getWidth() / 2), 100);
		// serverCode.setFocusable(false);
		serverCode.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Nytrone.DEFAULT_COLOR));
		serverCode.setFont(Nytrone.TAHOMA.deriveFont(25.0f));

		serverCodeReset = new JButton();
		serverCodeReset.setSize(24, 24);
		serverCodeReset.setRolloverEnabled(true);
		serverCodeReset.setRolloverIcon(new ImageIcon(Utils.getFileAsBufferedImage("resetBtnHover.png")));
		serverCodeReset.setIcon(new ImageIcon(Utils.getFileAsBufferedImage("resetBtn.png")));
		serverCodeReset.setContentAreaFilled(false);
		serverCodeReset.setFocusPainted(false);
		serverCodeReset.setBorderPainted(false);
		serverCodeReset.setLocation(((400 / 2) - (serverCode.getWidth() / 2)) + 125, 115);
		serverCodeReset.addActionListener((e) -> {
			final String randomCode = new RandomString(6).nextString();
			serverCode.setText(randomCode);
			FileManager.getInstance().getConfig().set("settings.server.code", randomCode);
			try {
				FileManager.getInstance().saveConfig();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		panel.add(serverCodeLabel);
		panel.add(serverCode);
		panel.add(serverCodeReset);

		panel.add(connectedComputersLabel);

		MouseListener mouseListener = new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				MenuSelectionManager.defaultManager().clearSelectedPath();
			}

		};
		
		for (Component c : getComponents()) {
			if (!(c instanceof JMenu)) {
				c.addMouseListener(mouseListener);
			}
		}
		for (Component c : panel.getComponents()) {
			if (!(c instanceof JMenu)) {
				c.addMouseListener(mouseListener);
			}
		}
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		Line2D lin = new Line2D.Float(getWidth() / 2, 35, getWidth() / 2, 500);
		g2d.setColor(Color.LIGHT_GRAY);
		g2d.draw(lin);
	}

	public NytroneFrame getFrame() {
		return frame;
	}

}
