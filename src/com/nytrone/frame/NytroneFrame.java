package com.nytrone.frame;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.nytrone.config.FileManager;
import com.nytrone.net.NetworkManager;
import com.nytrone.tray.TrayManager;
import com.nytrone.utils.Utils;

public class NytroneFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel page;
	private TrayManager trayManager;
	private FileManager fileManager;
	private NetworkManager networkManager;

	/**
	 * Create the frame.
	 */
	public NytroneFrame() {
		super("Nytrone");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600); //sets the size of the frame.
		setResizable(false);
		setIconImage(Utils.getFileAsBufferedImage("LogoSmall.png")); //this sets the icon image of the application.
		addWindowStateListener(new WindowStateListener() { //this minimizes the application to the tray when you minimize it.
			public void windowStateChanged(WindowEvent e) {
				if(e.getNewState() == Frame.ICONIFIED) {
					setVisible(false);
				}
			}
		});
		trayManager = new TrayManager(this); //inits the tray manager
		fileManager = FileManager.getInstance(); //inits the file manager
		fileManager.setup();
		networkManager = new NetworkManager(this);// innits the network manager;
	}

	public TrayManager getTrayManager() {
		return trayManager;
	}

	public NetworkManager getNetworkManager() {
		return networkManager;
	}
	
	public JPanel getPage() {
		return page;
	}

	//This function allows you to set the main 'Page' of the website which is just any JPanel.
	public void setPage(JPanel panel) {
		// setContentPane(panel);
		if (page != null) {
			page.invalidate();
			page.setVisible(false);
			page.removeAll();
			getContentPane().remove(page);
			page = null;
		}
		page = panel;
		add(panel);
	}

}
