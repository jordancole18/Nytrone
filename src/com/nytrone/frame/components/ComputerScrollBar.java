package com.nytrone.frame.components;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ComputerScrollBar extends JScrollPane {

	private static final long serialVersionUID = 1L;

	public ComputerScrollBar(JPanel panel) {
		super(panel);
		getVerticalScrollBar().setUnitIncrement(16);
	}

}