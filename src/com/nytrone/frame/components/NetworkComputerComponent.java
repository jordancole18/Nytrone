package com.nytrone.frame.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.nytrone.Nytrone;

public class NetworkComputerComponent extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private String computerName;
	
	public NetworkComputerComponent(String computerName) {
		setLayout(null);
		setSize(300, 50);
		setPreferredSize(new Dimension(300, 50));
		setMaximumSize(new Dimension(600, 50));
		setBackground(Nytrone.DEFAULT_COLOR);
		setForeground(Color.WHITE);
		JLabel label = new JLabel(computerName);
		label.setSize(125, 50);
		label.setLocation((this.getWidth() / 2) - (label.getWidth() / 2),
				((this.getHeight() / 2) - (label.getHeight() / 2)));
		label.setForeground(Color.WHITE);
		add(label);
		
		this.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.out.println("CLICKED");
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				setBackground(Nytrone.DEFAULT_COLOR.darker());
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				setBackground(Nytrone.DEFAULT_COLOR);
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				setBackground(Nytrone.DEFAULT_COLOR);
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				setBackground(Nytrone.DEFAULT_COLOR.darker());
			}
			
		});
		
	}
	
	public String getComputerName() {
		return computerName;
	}
	
}
