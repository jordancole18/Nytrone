package com.nytrone;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JLabel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.nytrone.frame.NytroneFrame;
import com.nytrone.frame.pages.SetupPage;
import com.nytrone.frame.pages.home.ClientPanel;
import com.nytrone.frame.pages.home.ServerPanel;
import com.nytrone.net.NetworkType;
import com.nytrone.utils.Utils;

public class Nytrone {

	public static final Font DEFAULT_FONT, INSTALL_FONT;
	public static final Font TAHOMA;
	public static final Color DEFAULT_COLOR;
	public static final Logger logger = Logger.getLogger(Nytrone.class);

	static {
		InputStream stream = Utils.getFileAsStream("Helvetica.ttf");
		InputStream tStream = Utils.getFileAsStream("Tahoma.ttf");
		InputStream ifStream = Utils.getFileAsStream("InstallFont.ttf");
		Font f = new JLabel().getFont();
		Font tahoma = new JLabel().getFont();
		Font installFont = new JLabel().getFont();
		try {
			f = Font.createFont(Font.TRUETYPE_FONT, stream);
			tahoma = Font.createFont(Font.TRUETYPE_FONT, tStream);
			installFont = Font.createFont(Font.TRUETYPE_FONT, ifStream);
		} catch (FontFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DEFAULT_FONT = f.deriveFont(24.0f);
		TAHOMA = tahoma.deriveFont(11.0f);
		INSTALL_FONT = installFont.deriveFont(24.0f);
		DEFAULT_COLOR = new Color(93, 188, 210);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					org.apache.log4j.BasicConfigurator.configure();
					Logger.getRootLogger().setLevel(Level.ALL);
					
					NytroneFrame frame = new NytroneFrame();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
					if (frame.getNetworkManager().init()) {
						NetworkType type = frame.getNetworkManager().getType();
						if (type == NetworkType.SERVER) {
							frame.setPage(new ServerPanel(frame));
						} else {
							frame.setPage(new ClientPanel(frame));
						}
					} else {
						frame.setPage(new SetupPage(frame));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
